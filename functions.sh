#!/bin/bash

# Function to create all the directories needed
# Argument is an array of directories to create, defaulting to data and output
function setup_directories {
    # Define "dirs" variable that is either the specified argument ($1),
    # or if it doesn't exist (-), defined as an array of "data" and "output"
    dirs=${@:-"data output"}
    
    # Create directories (without error if it already exists)
    # Stop the script if there is an error
    mkdir -p ${dirs} || exit 1
}


function ndvi_calc {
	lsatfile=$1
    	outfile=$2
    	filetemp1=${3:-temp1.tif}
    	pxsize=${4:-60}

	gdal_calc.py -A $lsatfile --A_band=4 -B $lsatfile --B_band=3 --outfile=$filetemp1 --calc="(A.astype(float)-B)/(A.astype(float)+B)" --type='Float32'
	
	gdalwarp -tr $pxsize $pxsize -wo "STREAMABLE_OUTPUT=TRUE" $filetemp1 $outfile|gdalwarp -t_srs WGS84 $filetemp1 $outfile
	
	
	}
	

  
